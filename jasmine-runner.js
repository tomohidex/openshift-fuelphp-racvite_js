/*
from
http://qiita.com/hosomichi/items/4e508e804b0e50e7b6ec

node jasmine-runner.js
or
npm test
*/
var Jasmine = require('jasmine');
var SpecReporter = require('jasmine-spec-reporter');
var noop = function() {};

var jrunner = new Jasmine();
jrunner.configureDefaultReporter({print: noop});
jasmine.getEnv().addReporter(new SpecReporter());
jrunner.loadConfigFile();
jrunner.execute();
