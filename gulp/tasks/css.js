/**
 * Created by tomohidex on 2015/07/12.
 */
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css');

/*
 gulp css
 */
gulp.task('css', function () {
    gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.min.css'
    ])
        .pipe(gulp.dest('./public/assets/css/lib/'))
        .pipe(concat('libs.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./public/assets/css/build/'));
});